import java.util.*;

public class JobSchedule{
	ArrayList<Job> jobs = new ArrayList<>();
	private Job dummyJob = new Job(0);
	private boolean modified = false;

	public Job addJob(int time){
		Job j = new Job(time);
		jobs.add(j); 
		relax(j, dummyJob);
		return j;
	}

	public Job getJob(int index){ return jobs.get(index); }

	public int minCompletionTime(){
		if(modified){
			topSort(jobs);
			modified = false;
		}
		return dummyJob.startTime;
	}

	private ArrayList<Job> topSort(ArrayList<Job> jobs){
		ArrayList<Job> topOrder = new ArrayList<Job>();
		
		for(Job j : jobs){
			j.finished = false;
			if((j.mutableInDegree = j.inDegree) == 0)
				topOrder.add(j);
		}
		
		for(int i = 0; i < topOrder.size(); i++){
			Job job = topOrder.get(i);
			for(Job j : job.outGoingList){
				if(--j.mutableInDegree == 0)
					topOrder.add(j);
				if(relax(job, j))
					relax(j,dummyJob);
			}
			job.finished = true;
		}

		if(topOrder.size() != jobs.size()){
			dummyJob.startTime = -1;
			for(Job j : jobs)
				if(!j.finished)
					j.startTime = -1;
		}
		return topOrder;
	}

	private	boolean relax(Job j0, Job j1){
		if(j0.startTime + j0.completionTime > j1.startTime){
			j1.startTime = j0.startTime + j0.completionTime;
			return true;
		}return false;
	}

	public class Job{
		private int completionTime, startTime = 0, inDegree = 0, mutableInDegree = 0;
		private boolean finished = false;
		private ArrayList<Job> outGoingList = new ArrayList<>();;

		private Job(int completionTime){ this.completionTime = completionTime; }

		public void requires(Job j){
			j.outGoingList.add(this);
			inDegree++;
			modified = true;
		}

		public int getStartTime(){
			if(modified){
				topSort(jobs);
				modified = false;
			}
			return startTime;
		}

		public int getCompletionTime(){ return completionTime; }	
	}
}